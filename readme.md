<p align="center">
  <a href="" rel="noopener">
 <img  height=100px src="https://cdn.shopify.com/s/files/1/1740/0017/files/puffy-mattress-main-logo_410x.png?v=7486112297402488452" alt="Project logo"></a>
</p>

<h2 align="center"><a href="https://puffy.com/">Puffy</a></h2>

---

<p align="center"> Test project to explore the capabilities of scss , html and gulp.
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>

Test project to explore the capabilities of scss , html and gulp.
using mobile first style methodolody and BEM .


## 🏁 Getting Started <a name = "getting_started"></a>

Download project or clone from gitlab

```
git clone https://gitlab.com/shahbazmancho/puffy.git

```

### Installing

Once you have the project on your machine simply install the required modules by using yarn or npm

```
yarn install
```

OR

```
npm install
```

## ⛏️ Built Using <a name = "built_using"></a>

- [Sass](https://sass-lang.com/)
- [HTML](#)
- [GulpJs](https://gulpjs.com/) - Library
- [NodeJs](https://nodejs.org/en/) - Server Environment
## ✍️ Authors <a name = "authors"></a>

- [@shahbazmancho](https://gitlab.com/shahbazmancho)



